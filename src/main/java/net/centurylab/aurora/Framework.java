package net.centurylab.aurora;

import com.google.common.base.Preconditions;

import org.bukkit.plugin.java.JavaPlugin;

import net.centurylab.aurora.annotations.NoToString;
import net.centurylab.aurora.database.Database;
import net.centurylab.aurora.singleton.SingletonManager;
import net.centurylab.aurora.threading.ThreadPool;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class Framework
{
    private static Database database;

    public static void init(JavaPlugin plugin, Database database, int numberOfThreads) throws IllegalAccessException
    {
        SingletonManager.setPlugin(plugin);

        if (database != null)
        {
            Preconditions.checkArgument(numberOfThreads > 0);

            ThreadPool.init(numberOfThreads);

            Framework.database = database;
        }

    }

    public static Database getDatabase()
    {
        return database;
    }

    public static String toStringHelper(Object o)
    {
        if (o == null)
        {
            return "null";
        }

        if (o instanceof String)
        {
            return (String) o;
        }
        else if (o instanceof Number)
        {
            return o.toString();
        }
        else if (o instanceof Map)
        {
            return String.format("%s{size=%d}", o.getClass().getSimpleName(), ((Map) o).size());
        }
        else if (o instanceof List)
        {
            return String.format("%s{size=%d}", o.getClass().getSimpleName(), ((List) o).size());
        }

        StringBuilder stringBuilder = new StringBuilder();

        Class aClass = o.getClass();
        Class superClass = o.getClass().getSuperclass();

        int size;
        int currentLength = 0;
        List<Field> fields = new ArrayList<>();
        Object fieldResult;

        stringBuilder.append(String.format("%s{", aClass.getSimpleName()));

        Collections.addAll(fields, aClass.getDeclaredFields());

        while (superClass != null)
        {
            Collections.addAll(fields, superClass.getDeclaredFields());
            superClass = superClass.getSuperclass();
        }

        for (Field field : fields)
        {
            try
            {
                if (field.isAnnotationPresent(NoToString.class))
                {
                    continue;
                }

                if (!field.isAccessible())
                {
                    field.setAccessible(true);
                }

                fieldResult = field.get(o);


                stringBuilder.append(String.format((fieldResult instanceof Number ? "%s=%s, " : "%s='%s', "), field.getName(), (fieldResult == null ? "null" : fieldResult.toString())));
            }
            catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }
        }


        stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length()).append("}");

        return stringBuilder.toString();
    }
}
