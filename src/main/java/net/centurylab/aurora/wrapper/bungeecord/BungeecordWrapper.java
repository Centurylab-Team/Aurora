package net.centurylab.aurora.wrapper.bungeecord;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import org.bukkit.entity.Player;

import net.centurylab.aurora.singleton.SingletonManager;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class BungeecordWrapper
{
    public static void sendPlayerToServer(Player player, String serverName) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        if (serverName == null || serverName.equalsIgnoreCase(""))
        {
            throw new Exception("The serverName can't be null or must contain a value");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("Connect");
        output.writeUTF(serverName);

        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void sendPlayerToServer(String playerName, String serverName, Player invoker) throws Exception
    {
        if (invoker == null)
        {
            throw new Exception("The invoker can't be null");
        }

        if (serverName == null || serverName.equalsIgnoreCase(""))
        {
            throw new Exception("The serverName can't be null or must contain a value");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("ConnectOther");
        output.writeUTF(playerName);
        output.writeUTF(serverName);

        invoker.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void getIP(Player player) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("IP");
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void getPlayerCount(Player player, String serverName) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        if (serverName == null)
        {
            throw new Exception("The serverName can't be null");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("PlayerCount");
        output.writeUTF((serverName.equalsIgnoreCase("") ? "ALL" : serverName));
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void getPlayerList(Player player, String serverName) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        if (serverName == null)
        {
            throw new Exception("The serverName can't be null");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("PlayerList");
        output.writeUTF((serverName.equalsIgnoreCase("") ? "ALL" : serverName));
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void getServers(Player player) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("GetServers");
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void sendMessage(Player player, String playerName, String message) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        if (playerName == null || playerName.equalsIgnoreCase(""))
        {
            throw new Exception("The playerName can't be null or must contain a value");
        }

        if (message == null || message.equalsIgnoreCase(""))
        {
            throw new Exception("The message can't be null or must contain a value");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("Message");
        output.writeUTF(playerName);
        output.writeUTF(message);
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void getServer(Player player) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("GetServer");
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void forward(Player player, String subChannel, String serverName, Object... arguments) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        if (subChannel == null || subChannel.equalsIgnoreCase(""))
        {
            throw new Exception("The subChannel can't be null or must contain a value");
        }

        if (serverName == null || serverName.equalsIgnoreCase(""))
        {
            throw new Exception("The serverName can't be null or must contain a value");
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

        for (Object argument : arguments)
        {
            if (argument instanceof Boolean)
            {
                dataOutputStream.writeBoolean((Boolean) argument);
            }
            else if (argument instanceof Integer)
            {
                dataOutputStream.writeInt((Integer) argument);
            }
            else if (argument instanceof Character)
            {
                dataOutputStream.writeChar((Character) argument);
            }
            else if (argument instanceof Byte)
            {
                dataOutputStream.writeByte((Byte) argument);
            }
            else if (argument instanceof Short)
            {
                dataOutputStream.writeShort((Short) argument);
            }
            else if (argument instanceof Double)
            {
                dataOutputStream.writeDouble((Double) argument);
            }
            else if (argument instanceof Long)
            {
                dataOutputStream.writeLong((Long) argument);
            }
            else if (argument instanceof Float)
            {
                dataOutputStream.writeFloat((Float) argument);
            }
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("Forward");
        output.writeUTF(serverName);
        output.writeUTF(subChannel);
        output.writeShort(byteArrayOutputStream.toByteArray().length);
        output.write(byteArrayOutputStream.toByteArray());
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void forwardToPlayer(Player player, String playerName, String subChannel, Object... arguments) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        if (playerName == null || playerName.equalsIgnoreCase(""))
        {
            throw new Exception("The playerName can't be null or must contain a value");
        }

        if (subChannel == null || subChannel.equalsIgnoreCase(""))
        {
            throw new Exception("The subChannel can't be null or must contain a value");
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);

        for (Object argument : arguments)
        {
            if (argument instanceof Boolean)
            {
                dataOutputStream.writeBoolean((Boolean) argument);
            }
            else if (argument instanceof Integer)
            {
                dataOutputStream.writeInt((Integer) argument);
            }
            else if (argument instanceof Character)
            {
                dataOutputStream.writeChar((Character) argument);
            }
            else if (argument instanceof Byte)
            {
                dataOutputStream.writeByte((Byte) argument);
            }
            else if (argument instanceof Short)
            {
                dataOutputStream.writeShort((Short) argument);
            }
            else if (argument instanceof Double)
            {
                dataOutputStream.writeDouble((Double) argument);
            }
            else if (argument instanceof Long)
            {
                dataOutputStream.writeLong((Long) argument);
            }
            else if (argument instanceof Float)
            {
                dataOutputStream.writeFloat((Float) argument);
            }
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("Forward");
        output.writeUTF(playerName);
        output.writeUTF(subChannel);
        output.writeShort(byteArrayOutputStream.toByteArray().length);
        output.write(byteArrayOutputStream.toByteArray());
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void getUUID(Player player) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("UUID");
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void getUUID(Player player, String playerName) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        if (playerName == null || playerName.equalsIgnoreCase(""))
        {
            throw new Exception("The playerName can't be null or must contain a value");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("UUIDOther");
        output.writeUTF(playerName);
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void getServerIP(Player player, String serverName) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        if (serverName == null || serverName.equalsIgnoreCase(""))
        {
            throw new Exception("The serverName can't be null or must contain a value");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("ServerIP");
        output.writeUTF(serverName);
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }

    public static void kickPlayer(Player player, String playerName, String reason) throws Exception
    {
        if (player == null)
        {
            throw new Exception("The player can't be null");
        }

        if (playerName == null || playerName.equalsIgnoreCase(""))
        {
            throw new Exception("The playerName can't be null or must contain a value");
        }

        if (reason == null || reason.equalsIgnoreCase(""))
        {
            throw new Exception("The reason can't be null or must contain a value");
        }

        ByteArrayDataOutput output = ByteStreams.newDataOutput();
        output.writeUTF("KickPlayer");
        output.writeUTF(playerName);
        output.writeUTF(reason);
        player.sendPluginMessage(SingletonManager.getPlugin(), "BungeeCord", output.toByteArray());
    }
}
