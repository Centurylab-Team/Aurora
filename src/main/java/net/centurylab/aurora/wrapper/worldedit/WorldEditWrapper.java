package net.centurylab.aurora.wrapper.worldedit;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public class WorldEditWrapper
{
    private static WorldEditPlugin worldEditPlugin = null;

    public static void init() throws Exception
    {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");

        // WorldEdit may not be loaded
        if (plugin == null || !(plugin instanceof WorldEditPlugin))
        {
            Exception exception = new Exception("Can't find WorldEdit. Please download and put WorldEdit into your plugins directory");
            StackTraceElement stackTraceElement = new StackTraceElement("WorldEditWrapper", "init", "WorldEditWrapper.java", 18);
            StackTraceElement[] stackTraceElements = new StackTraceElement[1];
            stackTraceElements[0] = stackTraceElement;
            exception.setStackTrace(stackTraceElements);
            throw exception;
        }

        worldEditPlugin = (WorldEditPlugin) plugin;
    }
}
