package net.centurylab.aurora.wrapper.PermissionsEx;

import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import org.bukkit.entity.Player;

public class PermissionsExWrapper
{
    private PermissionUser permissionUser;

    public static String getPrefix(Player player)
    {
        String prefix = "";

        PermissionUser permissionUser = PermissionsEx.getUser(player);

        if (permissionUser == null)
        {
            return "";
        }

        int lastRank = Integer.MAX_VALUE;

        for (PermissionGroup permissionGroup : PermissionsEx.getPermissionManager().getGroupList())
        {
            if (permissionGroup.getUsers().contains(permissionUser) && permissionGroup.getRank() < lastRank)
            {
                prefix = permissionGroup.getPrefix();
                lastRank = permissionGroup.getRank();
            }
        }

        return prefix;
    }

    public static boolean has(Player player, String permission)
    {
        PermissionUser permissionUser = PermissionsEx.getUser(player);

        return permissionUser != null && permissionUser.has(permission);
    }
}
