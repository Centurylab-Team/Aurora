package net.centurylab.aurora.configuration;

import net.centurylab.aurora.singleton.SingletonManager;

public abstract class Configuration
{
    private ConfigManager configManager;

    public Configuration()
    {
        try
        {
            this.configManager = new ConfigManager(SingletonManager.getPlugin());
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void load()
    {
        this.configManager.loadConfigForClass(getClass());
    }

    public void save()
    {
        this.configManager.saveConfigForClass(getClass());
    }
}
