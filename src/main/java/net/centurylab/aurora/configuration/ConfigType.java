package net.centurylab.aurora.configuration;

public enum ConfigType
{
    OBJECT,
    BOOLEAN,
    BOOLEANLIST,
    BYTELIST,
    CHARACTERLIST,
    COLOR,
    DOUBLE,
    DOUBLELIST,
    FLOATLIST,
    INT,
    INTEGERLIST,
    ITEMSTACK,
    LIST,
    LONG,
    LONGLIST,
    MAPLIST,
    OFFLINEPLAYER,
    SHORTLIST,
    STRING,
    STRINGLIST,
    VECTOR,
    LOCATION
}
