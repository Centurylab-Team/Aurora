package net.centurylab.aurora.configuration;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class ConfigManager
{
    private JavaPlugin plugin;

    public ConfigManager(JavaPlugin plugin)
    {
        this.plugin = plugin;
    }

    public void loadConfigForClass(Class aClass)
    {
        YamlConfiguration configuration = null;
        String currentConfig = "";

        for (Field field : aClass.getDeclaredFields())
        {
            if (!field.isAnnotationPresent(ConfigKey.class))
            {
                continue;
            }

            ConfigKey configKey = field.getAnnotation(ConfigKey.class);

            if (!field.isAccessible())
            {
                field.setAccessible(true);
            }

            try
            {
                if (!currentConfig.equals(configKey.FILE()))
                {
                    File file = new File(this.plugin.getDataFolder(), configKey.FILE());

                    if (!file.exists())
                    {
                        saveConfigForClass(aClass);
                    }

                    configuration = YamlConfiguration.loadConfiguration(file);
                    currentConfig = configKey.FILE();
                }

                if (configuration == null)
                {
                    continue;
                }

                switch (configKey.TYPE())
                {
                    case LONG:
                        if (configuration.isSet(configKey.KEY()) && configuration.isLong(configKey.KEY()))
                        {
                            field.setLong(aClass, configuration.getLong(configKey.KEY(), 0));
                        }
                        break;
                    case INT:
                        if (configuration.isSet(configKey.KEY()) && configuration.isInt(configKey.KEY()))
                        {
                            field.setInt(aClass, configuration.getInt(configKey.KEY(), 0));
                        }
                        break;
                    case DOUBLE:
                        if (configuration.isSet(configKey.KEY()) && configuration.isDouble(configKey.KEY()))
                        {
                            field.setDouble(aClass, configuration.getDouble(configKey.KEY(), 0));
                        }
                        break;
                    case BOOLEAN:
                        if (configuration.isSet(configKey.KEY()) && configuration.isBoolean(configKey.KEY()))
                        {
                            field.setBoolean(aClass, configuration.getBoolean(configKey.KEY(), false));
                        }
                        break;
                    case OBJECT:
                        if (configuration.isSet(configKey.KEY()))
                        {
                            field.set(aClass, configuration.get(configKey.KEY()));
                        }
                        break;
                    case BOOLEANLIST:
                        if (configuration.isSet(configKey.KEY()) && configuration.isList(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getBooleanList(configKey.KEY()));
                        }
                        break;
                    case BYTELIST:
                        if (configuration.isSet(configKey.KEY()) && configuration.isList(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getByteList(configKey.KEY()));
                        }
                        break;
                    case CHARACTERLIST:
                        if (configuration.isSet(configKey.KEY()) && configuration.isList(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getCharacterList(configKey.KEY()));
                        }
                        break;
                    case COLOR:
                        if (configuration.isSet(configKey.KEY()) && configuration.isColor(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getColor(configKey.KEY()));
                        }
                        break;
                    case DOUBLELIST:
                        if (configuration.isSet(configKey.KEY()) && configuration.isList(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getDoubleList(configKey.KEY()));
                        }
                        break;
                    case FLOATLIST:
                        if (configuration.isSet(configKey.KEY()) && configuration.isList(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getFloatList(configKey.KEY()));
                        }
                        break;
                    case INTEGERLIST:
                        if (configuration.isSet(configKey.KEY()) && configuration.isList(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getIntegerList(configKey.KEY()));
                        }
                        break;
                    case ITEMSTACK:
                        if (configuration.isSet(configKey.KEY()) && configuration.isItemStack(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getItemStack(configKey.KEY()));
                        }
                        break;
                    case LIST:
                        if (configuration.isSet(configKey.KEY()) && configuration.isList(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getList(configKey.KEY()));
                        }
                        break;
                    case LONGLIST:
                        if (configuration.isSet(configKey.KEY()) && configuration.isList(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getLongList(configKey.KEY()));
                        }
                        break;
                    case MAPLIST:
                        if (configuration.isSet(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getMapList(configKey.KEY()));
                        }
                        break;
                    case OFFLINEPLAYER:
                        if (configuration.isSet(configKey.KEY()) && configuration.isOfflinePlayer(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getOfflinePlayer(configKey.KEY()));
                        }
                        break;
                    case SHORTLIST:
                        if (configuration.isSet(configKey.KEY()) && configuration.isList(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getShortList(configKey.KEY()));
                        }
                        break;
                    case STRING:
                        if (configuration.isSet(configKey.KEY()) && configuration.isString(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getString(configKey.KEY(), ""));
                        }
                        break;
                    case STRINGLIST:
                        if (configuration.isSet(configKey.KEY()) && configuration.isList(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getStringList(configKey.KEY()));
                        }
                        break;
                    case VECTOR:
                        if (configuration.isSet(configKey.KEY()) && configuration.isVector(configKey.KEY()))
                        {
                            field.set(aClass, configuration.getVector(configKey.KEY()));
                        }
                        break;
                    case LOCATION:
                        if (configuration.isSet(configKey.KEY() + ".WORLD")
                                && configuration.isSet(configKey.KEY() + ".X")
                                && configuration.isSet(configKey.KEY() + ".Y")
                                && configuration.isSet(configKey.KEY() + ".Z")
                                && configuration.isSet(configKey.KEY() + ".YAW")
                                && configuration.isSet(configKey.KEY() + ".PITCH")
                                && configuration.isString(configKey.KEY() + ".world")
                                && configuration.isDouble(configKey.KEY() + ".X")
                                && configuration.isDouble(configKey.KEY() + ".Y")
                                && configuration.isDouble(configKey.KEY() + ".Z"))
                        {
                            field.set(aClass, new Location(Bukkit.getWorld(configuration.getString(configKey.KEY() + ".WORLD")),
                                    configuration.getDouble(configKey.KEY() + ".X"),
                                    configuration.getDouble(configKey.KEY() + ".Y"),
                                    configuration.getDouble(configKey.KEY() + ".Z"),
                                    Float.valueOf(configuration.getString(configKey.KEY() + ".YAW")),
                                    Float.valueOf(configuration.getString(configKey.KEY() + ".PITCH"))));
                        }

                        break;
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    public void saveConfigForClass(Class aClass)
    {
        Map<String, Map<String, Object>> files = new HashMap<>();

        for (Field field : aClass.getDeclaredFields())
        {
            if (!field.isAnnotationPresent(ConfigKey.class))
            {
                continue;
            }

            ConfigKey configKey = field.getAnnotation(ConfigKey.class);

            if (!files.containsKey(configKey.FILE()))
            {
                files.put(configKey.FILE(), new HashMap<String, Object>());
            }

            try
            {
                switch (configKey.TYPE())
                {

                    case BOOLEAN:
                        files.get(configKey.FILE()).put(configKey.KEY(), field.getBoolean(aClass));
                        break;
                    case INT:
                        files.get(configKey.FILE()).put(configKey.KEY(), field.getInt(aClass));
                        break;
                    case LONG:
                        files.get(configKey.FILE()).put(configKey.KEY(), field.getLong(aClass));
                        break;
                    case DOUBLE:
                        files.get(configKey.FILE()).put(configKey.KEY(), field.getDouble(aClass));
                        break;
                    case LOCATION:
                        Location location = (Location) field.get(aClass);

                        files.get(configKey.FILE()).put(configKey.KEY() + ".world", location.getWorld().getName());
                        files.get(configKey.FILE()).put(configKey.KEY() + ".X", location.getX());
                        files.get(configKey.FILE()).put(configKey.KEY() + ".Y", location.getY());
                        files.get(configKey.FILE()).put(configKey.KEY() + ".Z", location.getZ());
                        files.get(configKey.FILE()).put(configKey.KEY() + ".YAW", location.getYaw());
                        files.get(configKey.FILE()).put(configKey.KEY() + ".PITCH", location.getPitch());
                        break;
                    case OBJECT:
                    case BOOLEANLIST:
                    case BYTELIST:
                    case CHARACTERLIST:
                    case COLOR:
                    case DOUBLELIST:
                    case FLOATLIST:
                    case INTEGERLIST:
                    case ITEMSTACK:
                    case LIST:
                    case LONGLIST:
                    case MAPLIST:
                    case OFFLINEPLAYER:
                    case SHORTLIST:
                    case STRING:
                    case STRINGLIST:
                    case VECTOR:
                        files.get(configKey.FILE()).put(configKey.KEY(), field.get(aClass));
                        break;
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        YamlConfiguration configuration;

        for (Map.Entry<String, Map<String, Object>> entry : files.entrySet())
        {
            File file = new File(this.plugin.getDataFolder(), entry.getKey());
            configuration = YamlConfiguration.loadConfiguration(file);

            for (Map.Entry<String, Object> fileEntry : entry.getValue().entrySet())
            {
                configuration.set(fileEntry.getKey(), fileEntry.getValue());
            }

            try
            {
                configuration.save(file);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
