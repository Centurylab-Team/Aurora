package net.centurylab.aurora.minigames;

import org.bukkit.GameMode;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class AbstractMinigame extends JavaPlugin
{
    private MinigameState minigameState = MinigameState.NOT_STARTED;

    private GameMode spectatorGamemode = GameMode.SPECTATOR;

    public MinigameState getMinigameState()
    {
        return minigameState;
    }

    public void setMinigameState(MinigameState minigameState)
    {
        this.minigameState = minigameState;
    }

    public GameMode getSpectatorGamemode()
    {
        return spectatorGamemode;
    }

    public void setSpectatorGamemode(GameMode spectatorGamemode)
    {
        this.spectatorGamemode = spectatorGamemode;
    }
}
