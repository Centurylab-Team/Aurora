package net.centurylab.aurora.minigames;

public enum MinigameState
{
    NOT_STARTED(0),
    WAITING_FOR_PLAYERS(1),
    LOBBY(2),
    TELEPORTING_PLAYERS(3),
    PEACEFUL_PHASE(4),
    FIGHT_PHASE(5),
    DEATHMATCH(6),
    ENDED(7),
    END_LOBBY(8);

    private int value;

    MinigameState(int value)
    {
        this.value = value;
    }

    public int getValue()
    {
        return value;
    }
}
