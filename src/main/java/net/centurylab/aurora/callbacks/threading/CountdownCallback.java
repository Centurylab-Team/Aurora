package net.centurylab.aurora.callbacks.threading;

public interface CountdownCallback
{
    void onTick(int secondsRemaining);
}
