package net.centurylab.aurora.callbacks.database;

import net.centurylab.aurora.database.DatabaseResult;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public abstract class DatabaseCallback
{
    private Connection        connection;
    private PreparedStatement preparedStatement;
    private DatabaseResult    databaseResult;

    public abstract void onSuccess(ResultSet resultSet);

    public abstract void onFailure(Throwable error);

    public Connection getConnection()
    {
        return connection;
    }

    public void setConnection(Connection connection)
    {
        this.connection = connection;
    }

    public PreparedStatement getPreparedStatement()
    {
        return preparedStatement;
    }

    public void setPreparedStatement(PreparedStatement preparedStatement)
    {
        this.preparedStatement = preparedStatement;
    }

    public DatabaseResult getDatabaseResult()
    {
        return databaseResult;
    }

    public void setDatabaseResult(DatabaseResult databaseResult)
    {
        this.databaseResult = databaseResult;
    }
}
