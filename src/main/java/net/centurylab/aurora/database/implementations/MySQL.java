package net.centurylab.aurora.database.implementations;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.JdkFutureAdapters;
import com.google.common.util.concurrent.ListenableFuture;

import org.bukkit.Bukkit;

import net.centurylab.aurora.callbacks.database.DatabaseCallback;
import net.centurylab.aurora.database.Database;
import net.centurylab.aurora.database.DatabaseResult;
import net.centurylab.aurora.database.QueryType;
import net.centurylab.aurora.database.StatementBuilder;
import net.centurylab.aurora.singleton.SingletonManager;
import net.centurylab.aurora.threading.ThreadPool;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Callable;

public class MySQL extends Database
{
    private static final String driverClass = "com.mysql.jdbc.Driver";
    private static final String jdbcUrl     = "jdbc:mysql://%s:%s/%s";

    public MySQL(String host, int port, String database, String username, String password)
    {
        super(driverClass, String.format(jdbcUrl, host, port, database), username, password, 5, 10);
    }

    public MySQL(String host, int port, String database, String username, String password, int minPoolSize, int maxPoolSize)
    {
        super(driverClass, String.format(jdbcUrl, host, port, database), username, password, minPoolSize, maxPoolSize);
    }

    public MySQL(String host, String database, String username, String password)
    {
        super(driverClass, String.format(jdbcUrl, host, 3306, database), username, password, 5, 10);
    }

    public MySQL(String host, String database, String username, String password, int minPoolSize, int maxPoolSize)
    {
        super(driverClass, String.format(jdbcUrl, host, 3306, database), username, password, minPoolSize, maxPoolSize);
    }

    @Override
    public void execute(final StatementBuilder statementBuilder, final DatabaseCallback callback)
    {
        final ResultSet[] resultSets = new ResultSet[1];

        try
        {
            callback.setPreparedStatement(statementBuilder.build());
            callback.setConnection(callback.getPreparedStatement().getConnection());

            final Callable<DatabaseResult> databaseResultFutureTask;

            if (statementBuilder.getQueryType() == QueryType.SELECT)
            {
                databaseResultFutureTask = () ->
                {
                    try
                    {
                        callback.setDatabaseResult(new DatabaseResult());

                        resultSets[0] = callback.getPreparedStatement().executeQuery();

                        callback.getDatabaseResult().setResultSet(resultSets[0]);

                        return callback.getDatabaseResult();
                    }
                    catch (SQLException e)
                    {
                        callback.onFailure(e);

                        if (callback.getDatabaseResult() != null)
                        {
                            callback.getDatabaseResult().close(callback.getConnection(), callback.getPreparedStatement());
                        }
                        else
                        {
                            try
                            {
                                if (callback.getPreparedStatement() != null && !callback.getPreparedStatement().isClosed())
                                {
                                    callback.getPreparedStatement().close();
                                }

                                if (callback.getConnection() != null && !callback.getConnection().isClosed())
                                {
                                    callback.getConnection().close();
                                }
                            }
                            catch (SQLException e1)
                            {
                                callback.onFailure(e1);
                            }
                        }
                    }

                    return null;
                };
            }
            else
            {
                databaseResultFutureTask = () ->
                {
                    try
                    {
                        callback.setDatabaseResult(new DatabaseResult());

                        callback.getDatabaseResult().setAffectedRows(callback.getPreparedStatement().executeUpdate());

                        return callback.getDatabaseResult();
                    }
                    catch (SQLException e)
                    {
                        callback.onFailure(e);

                        if (callback.getDatabaseResult() != null)
                        {
                            callback.getDatabaseResult().close(callback.getConnection(), callback.getPreparedStatement());
                        }
                        else
                        {
                            try
                            {
                                if (callback.getPreparedStatement() != null && !callback.getPreparedStatement().isClosed())
                                {
                                    callback.getPreparedStatement().close();
                                }

                                if (callback.getConnection() != null && !callback.getConnection().isClosed())
                                {
                                    callback.getConnection().close();
                                }
                            }
                            catch (SQLException e1)
                            {
                                callback.onFailure(e1);
                            }
                        }
                    }

                    return null;
                };

            }

            ListenableFuture<DatabaseResult> listenableFuture = JdkFutureAdapters.listenInPoolThread(ThreadPool.getInstance().submit(databaseResultFutureTask), this.getThreadPool());

            Futures.addCallback(listenableFuture, new FutureCallback<DatabaseResult>()
            {
                @Override
                public void onSuccess(final DatabaseResult result)
                {
                    if (result == null)
                    {
                        return;
                    }

                    try
                    {
                        Bukkit.getScheduler().runTask(SingletonManager.getPlugin(), () ->
                        {
                            callback.onSuccess(callback.getDatabaseResult().getResultSet());

                            if (callback.getDatabaseResult() != null)
                            {
                                callback.getDatabaseResult().close(callback.getConnection(), callback.getPreparedStatement());
                            }
                            else
                            {
                                try
                                {
                                    if (callback.getPreparedStatement() != null && !callback.getPreparedStatement().isClosed())
                                    {
                                        callback.getPreparedStatement().close();
                                    }

                                    if (callback.getConnection() != null && !callback.getConnection().isClosed())
                                    {
                                        callback.getConnection().close();
                                    }
                                }
                                catch (SQLException e)
                                {
                                    callback.onFailure(e);
                                }
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        callback.onFailure(e);

                        if (callback.getDatabaseResult() != null)
                        {
                            callback.getDatabaseResult().close(callback.getConnection(), callback.getPreparedStatement());
                        }
                        else
                        {
                            try
                            {
                                if (callback.getPreparedStatement() != null && !callback.getPreparedStatement().isClosed())
                                {
                                    callback.getPreparedStatement().close();
                                }

                                if (callback.getConnection() != null && !callback.getConnection().isClosed())
                                {
                                    callback.getConnection().close();
                                }
                            }
                            catch (SQLException e1)
                            {
                                callback.onFailure(e1);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(final Throwable t)
                {
                    try
                    {
                        Bukkit.getScheduler().runTask(SingletonManager.getPlugin(), () ->
                        {
                            callback.onFailure(t);

                            if (callback.getDatabaseResult() != null)
                            {
                                callback.getDatabaseResult().close(callback.getConnection(), callback.getPreparedStatement());
                            }
                            else
                            {
                                try
                                {
                                    if (callback.getPreparedStatement() != null && !callback.getPreparedStatement().isClosed())
                                    {
                                        callback.getPreparedStatement().close();
                                    }

                                    if (callback.getConnection() != null && !callback.getConnection().isClosed())
                                    {
                                        callback.getConnection().close();
                                    }
                                }
                                catch (SQLException e)
                                {
                                    callback.onFailure(e);
                                }
                            }
                        });
                    }
                    catch (Exception e)
                    {
                        callback.onFailure(e);

                        if (callback.getDatabaseResult() != null)
                        {
                            callback.getDatabaseResult().close(callback.getConnection(), callback.getPreparedStatement());
                        }
                        else
                        {
                            try
                            {
                                if (callback.getPreparedStatement() != null && !callback.getPreparedStatement().isClosed())
                                {
                                    callback.getPreparedStatement().close();
                                }

                                if (callback.getConnection() != null && !callback.getConnection().isClosed())
                                {
                                    callback.getConnection().close();
                                }
                            }
                            catch (SQLException e1)
                            {
                                callback.onFailure(e1);
                            }
                        }
                    }
                }
            });
        }
        catch (Exception e)
        {
            callback.onFailure(e);

            if (callback.getDatabaseResult() != null)
            {
                callback.getDatabaseResult().close(callback.getConnection(), callback.getPreparedStatement());
            }
            else
            {
                try
                {
                    if (callback.getPreparedStatement() != null && !callback.getPreparedStatement().isClosed())
                    {
                        callback.getPreparedStatement().close();
                    }

                    if (callback.getConnection() != null && !callback.getConnection().isClosed())
                    {
                        callback.getConnection().close();
                    }
                }
                catch (SQLException e1)
                {
                    callback.onFailure(e1);
                }
            }
        }
    }
}
