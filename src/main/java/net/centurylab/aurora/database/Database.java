package net.centurylab.aurora.database;

import com.zaxxer.hikari.HikariDataSource;

import net.centurylab.aurora.callbacks.database.DatabaseCallback;

import java.sql.Connection;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public abstract class Database
{
    private static final HikariDataSource DATA_SOURCE = new HikariDataSource();

    private String          driverClass;
    private String          jdbcUrl;
    private String          username;
    private String          password;
    private int             minPoolSize;
    private int             maxPoolSize;
    private ExecutorService threadPool;

    public Database(String driverClass, String jdbcUrl, String username, String password, int minPoolSize, int maxPoolSize)
    {
        try
        {
            Class.forName(driverClass);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }

        this.driverClass = driverClass;
        this.jdbcUrl = jdbcUrl;
        this.username = username;
        this.password = password;
        this.minPoolSize = minPoolSize;
        this.maxPoolSize = maxPoolSize;
        this.threadPool = Executors.newCachedThreadPool();

        this.connect();
    }

    /**
     * @see HikariDataSource#getConnection()
     */
    public Connection getConnection()
    {
        try
        {
            return DATA_SOURCE.getConnection();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return null;
    }

    public ExecutorService getThreadPool()
    {
        return threadPool;
    }

    public void setThreadPool(ExecutorService threadPool)
    {
        this.threadPool = threadPool;
    }

    public abstract void execute(StatementBuilder statementBuilder, final DatabaseCallback callback);

    /**
     * Connects to the datasource through Hikari
     */
    private void connect()
    {
        DATA_SOURCE.setDriverClassName(this.driverClass);
        DATA_SOURCE.setJdbcUrl(this.jdbcUrl);
        DATA_SOURCE.setUsername(this.username);
        DATA_SOURCE.setPassword(this.password);

        Properties properties = new Properties();
        properties.setProperty("dataSource.cachePrepStmts", "true");
        properties.setProperty("dataSource.prepStmtCacheSize", "250");
        properties.setProperty("dataSource.prepStmtCacheSqlLimit", "2048");
        properties.setProperty("dataSource.dumpQueriesOnException", "true");

        DATA_SOURCE.setDataSourceProperties(properties);

        DATA_SOURCE.setConnectionTestQuery("SELECT 1");
        DATA_SOURCE.setConnectionTimeout(TimeUnit.SECONDS.toMillis(15));

        DATA_SOURCE.setMinimumIdle(this.minPoolSize);
        DATA_SOURCE.setMaximumPoolSize(this.maxPoolSize);
    }

    /**
     * Closes the pool
     */
    private void close()
    {
        DATA_SOURCE.close();
    }
}
