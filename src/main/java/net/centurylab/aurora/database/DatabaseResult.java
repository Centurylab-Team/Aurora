package net.centurylab.aurora.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class DatabaseResult
{

    private ResultSet resultSet;
    private int       affectedRows;
    private int       rowCount;

    public DatabaseResult()
    {
        this.resultSet = null;
        this.affectedRows = 0;
        this.rowCount = 0;
    }

    public ResultSet getResultSet()
    {
        return resultSet;
    }

    public void setResultSet(ResultSet resultSet)
    {
        this.resultSet = resultSet;

        try
        {
            this.rowCount = resultSet.last() ? resultSet.getRow() : 0;
            resultSet.beforeFirst();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            this.rowCount = 0;
        }
    }

    public void close(Connection connection, PreparedStatement preparedStatement)
    {
        try
        {
            if (resultSet != null && !resultSet.isClosed())
            {
                resultSet.close();
            }

            if (preparedStatement != null && !preparedStatement.isClosed())
            {
                preparedStatement.close();
            }

            if (connection != null && !connection.isClosed())
            {
                connection.close();
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public int getAffectedRows()
    {
        return affectedRows;
    }

    public void setAffectedRows(int affectedRows)
    {
        this.affectedRows = affectedRows;
    }

    public int getRowCount()
    {
        return rowCount;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof DatabaseResult))
        {
            return false;
        }
        DatabaseResult that = (DatabaseResult) o;
        return Objects.equals(getAffectedRows(), that.getAffectedRows()) &&
                Objects.equals(getRowCount(), that.getRowCount()) &&
                Objects.equals(getResultSet(), that.getResultSet());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getResultSet(), getAffectedRows(), getRowCount());
    }
}
