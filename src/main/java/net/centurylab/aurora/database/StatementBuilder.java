package net.centurylab.aurora.database;

import com.google.common.base.Preconditions;

import org.bukkit.entity.Player;

import net.centurylab.aurora.Framework;
import net.centurylab.aurora.singleton.SingletonManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class StatementBuilder
{
    private final List<Condition> conditions = new ArrayList<>();
    private final List<String>    fields     = new ArrayList<>();
    private final List<String>    tables     = new ArrayList<>();

    private QueryType queryType     = QueryType.SELECT;
    private String    currentSql    = "";
    private boolean   useConditions = false;
    private boolean   useLimits     = false;
    private int       limit         = 0;
    private int       offset        = 0;
    private boolean   useOrdering   = false;
    private OrderType orderType     = OrderType.ASCENDING;
    private String    orderField    = "";

    private Map<String, Object> values;

    private StatementBuilder()
    {

    }

    public static StatementBuilder Select()
    {
        StatementBuilder statementBuilder = new StatementBuilder();
        statementBuilder.queryType = QueryType.SELECT;

        return statementBuilder;
    }

    public static StatementBuilder Insert(Map<String, Object> values)
    {
        Preconditions.checkNotNull(values, "values can't be null");

        StatementBuilder statementBuilder = new StatementBuilder();

        statementBuilder.queryType = QueryType.INSERT;
        statementBuilder.values = values;

        return statementBuilder;
    }

    public static StatementBuilder Update(Map<String, Object> values)
    {
        Preconditions.checkNotNull(values, "values can't be null");

        StatementBuilder statementBuilder = new StatementBuilder();

        statementBuilder.queryType = QueryType.UPDATE;
        statementBuilder.values = values;

        return statementBuilder;
    }

    public static StatementBuilder Delete()
    {
        StatementBuilder statementBuilder = new StatementBuilder();
        statementBuilder.queryType = QueryType.DELETE;

        return statementBuilder;
    }

    public StatementBuilder addField(String fieldName)
    {
        this.fields.add(fieldName);
        return this;
    }

    public StatementBuilder addTable(String tableName)
    {
        this.tables.add(tableName);
        return this;
    }

    /**
     * Resets all variables
     */
    public StatementBuilder reset()
    {
        this.queryType = QueryType.SELECT;
        this.useConditions = false;
        this.conditions.clear();
        this.currentSql = "";
        this.useLimits = false;
        this.limit = 0;
        this.offset = 0;
        this.useOrdering = false;
        this.orderField = "";
        this.orderType = OrderType.ASCENDING;

        return this;
    }

    /**
     * Set to true if you want to use conditions for the next query
     *
     * @param useConditions True if you want to use conditions
     */
    public StatementBuilder useConditions(boolean useConditions)
    {
        this.useConditions = useConditions;

        return this;
    }

    /**
     * Adds a condition
     *
     * @param condition Condition to add
     * @throws IllegalArgumentException if the condition is null
     */
    public StatementBuilder addCondition(Condition condition) throws IllegalArgumentException
    {
        Preconditions.checkNotNull(condition, "Condition can't be null");

        this.useConditions = true;
        this.conditions.add(condition);

        return this;
    }

    /**
     * Deletes the condition on the given index
     *
     * @param index the index of the element to be removed
     * @throws IllegalArgumentException if the index is out of range
     */
    public StatementBuilder deleteCondition(int index) throws IllegalArgumentException
    {
        Preconditions.checkArgument(index > 0, "Index can't be negative");
        Preconditions.checkArgument(index < this.conditions.size() - 1, "The index can not be larger than the list");

        this.conditions.remove(index);

        return this;
    }

    /**
     * Deletes the condition by object
     *
     * @param condition Condition to delete
     * @throws IllegalArgumentException if the condition is null
     */
    public StatementBuilder deleteCondition(Condition condition) throws IllegalArgumentException
    {
        Preconditions.checkNotNull(condition, "Condition can't be null");

        this.conditions.remove(condition);

        return this;
    }

    /**
     * Builds all conditions for the query
     */
    protected void buildConditions()
    {
        Preconditions.checkArgument(this.useConditions, "You need to use conditions before you can build them");
        Preconditions.checkArgument(this.conditions.size() > 0, "You need to add conditions");
        this.conditions.forEach(Preconditions::checkNotNull);

        this.currentSql += " WHERE ";

        for (Condition condition : this.conditions)
        {
            if (condition.getClause().trim().equalsIgnoreCase("FIRST"))
            {
                this.currentSql += String.format("`%s` %s ?", condition.getField(), condition.getOperator());
            }
            else
            {
                this.currentSql += String.format("%s `%s` %s ?", condition.getClause(), condition.getField(), condition.getOperator());
            }
        }

    }

    /**
     * Limits the result to the given limit and starts at the given offset
     *
     * @param limit  Count of results
     * @param offset Index where to start
     * @throws IllegalArgumentException if limit or offset are negative
     */
    public StatementBuilder limit(int limit, int offset) throws IllegalArgumentException
    {
        Preconditions.checkArgument(limit > 0, "limit can't be negative or equalszero");

        Preconditions.checkArgument(offset > -1, "offset can't be negative");

        this.useLimits = true;
        this.limit = limit;
        this.offset = offset;

        return this;
    }

    /**
     * Orders the result by the given field by the given type
     *
     * @param orderField Name of the field
     * @param orderType  Type to order
     * @throws IllegalArgumentException if field equals "" or when the orderType is null
     */
    public StatementBuilder order(String orderField, OrderType orderType) throws IllegalArgumentException
    {
        orderField = orderField.trim();

        Preconditions.checkArgument(orderField.equalsIgnoreCase(""), "field can't be empty");
        Preconditions.checkNotNull(orderType, "orderType can't be null");

        this.useOrdering = true;
        this.orderField = orderField;
        this.orderType = orderType;

        return this;
    }

    public PreparedStatement build() throws Exception
    {
        SingletonManager.getPlugin().getLogger().info(String.format("tables size: %d", tables.size()));
        SingletonManager.getPlugin().getLogger().info(String.format("tables: %s", tables.toString()));
        Preconditions.checkArgument(tables.size() != 0, "tables can't be empty");

        switch (this.queryType)
        {
            case SELECT:
                this.currentSql = "SELECT ";

                if (fields.size() == 1)
                {
                    if (fields.get(0).equalsIgnoreCase("*"))
                    {
                        this.currentSql += String.format("%s", fields.get(0));
                    }
                    else
                    {
                        this.currentSql += String.format("`%s`", fields.get(0));
                    }
                }
                else
                {
                    for (int i = 0; i < fields.size(); i++)
                    {
                        if (i == fields.size() - 1)
                        {
                            this.currentSql += String.format("`%s`", fields.get(i));
                        }
                        else
                        {
                            this.currentSql += String.format("`%s`, ", fields.get(i));
                        }
                    }
                }

                this.currentSql += " FROM ";

                if (tables.size() == 1)
                {
                    this.currentSql += tables.get(0);
                }
                else
                {
                    for (int i = 0; i < tables.size(); i++)
                    {
                        if (i == tables.size() - 1)
                        {
                            this.currentSql += String.format("`%s`", tables.get(i));
                        }
                        else
                        {
                            this.currentSql += String.format("`%s`, ", tables.get(i));
                        }
                    }
                }

                if (this.useConditions)
                {
                    this.buildConditions();
                }

                if (this.useOrdering)
                {
                    this.currentSql += String.format(" ORDER BY `%s` %s", this.orderField, this.orderType.getValue());
                }

                if (this.useLimits)
                {
                    this.currentSql += String.format(" LIMIT %d", this.limit);

                    if (this.offset > 0)
                    {
                        this.currentSql += String.format(", %d", this.offset);
                    }
                }
                break;
            case INSERT:
                this.currentSql = String.format("INSERT INTO `%s` (", this.tables.get(0));
                for (int i = 0; i < values.keySet().size(); i++)
                {
                    if (i == values.keySet().size() - 1)
                    {
                        this.currentSql += String.format("`%s`", values.keySet().toArray()[i]);
                    }
                    else
                    {
                        this.currentSql += String.format("`%s`, ", values.keySet().toArray()[i]);
                    }
                }
                this.currentSql += ") VALUES (";
                for (int i = 0; i < values.values().size(); i++)
                {
                    if (i == values.values().size() - 1)
                    {
                        this.currentSql += "?";
                    }
                    else
                    {
                        this.currentSql += "?, ";
                    }
                }
                this.currentSql += ")";
                break;
            case DELETE:
                this.currentSql = String.format("DELETE FROM `%s`", tables.get(0));
                if (this.useConditions)
                {
                    this.buildConditions();
                }
                if (this.useLimits)
                {
                    this.currentSql += String.format(" LIMIT %d", this.limit);
                }
                break;
            case UPDATE:
                this.currentSql = String.format("UPDATE `%s` SET ", tables.get(0));
                for (int i = 0; i < values.keySet().size(); i++)
                {
                    if (i == values.keySet().size() - 1)
                    {
                        this.currentSql += String.format("`%s` = ?", values.keySet().toArray()[i]);
                    }
                    else
                    {
                        this.currentSql += String.format("`%s` = ?, ", values.keySet().toArray()[i]);
                    }
                }
                if (this.useConditions)
                {
                    this.buildConditions();
                }
                if (this.useLimits)
                {
                    this.currentSql += String.format(" LIMIT %d", this.limit);
                }
                break;
        }

        Connection connection = Framework.getDatabase().getConnection();

        Preconditions.checkNotNull(connection, "You have to initialize the framework with Framework.init(JavaPlugin plugin, Database database)");

        PreparedStatement statement = connection.prepareStatement(this.currentSql);

        if (this.currentSql.contains("?"))
        {
            if (useConditions && values != null)
            {
                int index = 0;

                for (int i = 0; index < values.values().size(); index++)
                {
                    Object o = values.values().toArray()[index];

                    if (o instanceof String)
                    {
                        statement.setString(index + 1, o.toString());
                    }
                    else if (o instanceof UUID)
                    {
                        statement.setString(index + 1, o.toString());
                    }
                    else if (o instanceof Player)
                    {
                        statement.setString(index + 1, ((Player) o).getUniqueId().toString());
                    }
                    else if (o instanceof Integer)
                    {
                        statement.setInt(index + 1, (Integer) o);
                    }
                    else if (o instanceof Float)
                    {
                        statement.setFloat(index + 1, (Float) o);
                    }
                    else if (o instanceof Long)
                    {
                        statement.setLong(index + 1, (Long) o);
                    }
                    else if (o instanceof Short)
                    {
                        statement.setShort(index + 1, (Short) o);
                    }
                    else
                    {
                        statement.setObject(index + 1, o);
                    }
                }

                index += 1;

                for (int i = 0; i < conditions.size(); i++)
                {
                    Condition condition = conditions.get(i);

                    if (condition.getValue() instanceof String)
                    {
                        statement.setString(i + index, condition.getValue().toString());
                    }
                    else if (condition.getValue() instanceof UUID)
                    {
                        statement.setString(i + index, condition.getValue().toString());
                    }
                    else if (condition.getValue() instanceof Player)
                    {
                        statement.setString(i + index, ((Player) condition.getValue()).getUniqueId().toString());
                    }
                    else if (condition.getValue() instanceof Integer)
                    {
                        statement.setInt(i + index, (Integer) condition.getValue());
                    }
                    else if (condition.getValue() instanceof Float)
                    {
                        statement.setFloat(i + index, (Float) condition.getValue());
                    }
                    else if (condition.getValue() instanceof Long)
                    {
                        statement.setLong(i + index, (Long) condition.getValue());
                    }
                    else if (condition.getValue() instanceof Short)
                    {
                        statement.setShort(i + index, (Short) condition.getValue());
                    }
                    else
                    {
                        statement.setObject(i + index, condition.getValue());
                    }
                }
            }
            else if (values != null && values.size() > 0)
            {
                for (int i = 0; i < values.values().size(); i++)
                {
                    Object o = values.values().toArray()[i];

                    if (o instanceof String)
                    {
                        statement.setString(i + 1, o.toString());
                    }
                    else if (o instanceof UUID)
                    {
                        statement.setString(i + 1, o.toString());
                    }
                    else if (o instanceof Player)
                    {
                        statement.setString(i + 1, ((Player) o).getUniqueId().toString());
                    }
                    else if (o instanceof Integer)
                    {
                        statement.setInt(i + 1, (Integer) o);
                    }
                    else if (o instanceof Float)
                    {
                        statement.setFloat(i + 1, (Float) o);
                    }
                    else if (o instanceof Long)
                    {
                        statement.setLong(i + 1, (Long) o);
                    }
                    else if (o instanceof Short)
                    {
                        statement.setShort(i + 1, (Short) o);
                    }
                    else
                    {
                        statement.setObject(i + 1, o);
                    }
                }
            }
            else if (useConditions && conditions.size() > 0)
            {
                for (int i = 0; i < conditions.size(); i++)
                {
                    Condition condition = conditions.get(i);

                    if (condition.getValue() instanceof String)
                    {
                        statement.setString(i + 1, condition.getValue().toString());
                    }
                    else if (condition.getValue() instanceof UUID)
                    {
                        statement.setString(i + 1, condition.getValue().toString());
                    }
                    else if (condition.getValue() instanceof Player)
                    {
                        statement.setString(i + 1, ((Player) condition.getValue()).getUniqueId().toString());
                    }
                    else if (condition.getValue() instanceof Integer)
                    {
                        statement.setInt(i + 1, (Integer) condition.getValue());
                    }
                    else if (condition.getValue() instanceof Float)
                    {
                        statement.setFloat(i + 1, (Float) condition.getValue());
                    }
                    else if (condition.getValue() instanceof Long)
                    {
                        statement.setLong(i + 1, (Long) condition.getValue());
                    }
                    else if (condition.getValue() instanceof Short)
                    {
                        statement.setShort(i + 1, (Short) condition.getValue());
                    }
                    else
                    {
                        statement.setObject(i + 1, condition.getValue());
                    }
                }
            }
        }

        return statement;
    }

    public QueryType getQueryType()
    {
        return queryType;
    }

    public String getCurrentSql()
    {
        return currentSql;
    }
}
