package net.centurylab.aurora.database;

public class Condition
{
    private String clause;
    private String operator;
    private String field;
    private Object value;

    public Condition()
    {
    }

    public Condition(String clause, String field, String operator, Object value)
    {
        this.clause = clause;
        this.operator = operator;
        this.field = field;
        this.value = value;
    }

    public Condition(String field, String operator, Object value)
    {
        this.clause = "FIRST";
        this.field = field;
        this.operator = operator;
        this.value = value;
    }

    public String getClause()
    {
        return clause;
    }

    public void setClause(String clause)
    {
        this.clause = clause;
    }

    public String getOperator()
    {
        return operator;
    }

    public void setOperator(String operator)
    {
        this.operator = operator;
    }

    public String getField()
    {
        return field;
    }

    public void setField(String field)
    {
        this.field = field;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }
}
