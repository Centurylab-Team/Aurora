package net.centurylab.aurora;

import org.bukkit.Bukkit;

import java.lang.reflect.Field;
import java.util.concurrent.Executor;

public class Aurora
{
    private static Executor mainThread = null;

    public static Executor mainThread()
    {
        if (Aurora.mainThread != null)
        {
            return Aurora.mainThread;
        }

        try
        {
            Class.forName(Bukkit.getScheduler().getClass().getName());

            for (Field field : Bukkit.getScheduler().getClass().getDeclaredFields())
            {
                if (field.getName().equalsIgnoreCase("executor"))
                {
                    if (!field.isAccessible())
                    {
                        field.setAccessible(true);
                        Aurora.mainThread = (Executor) field.get(Bukkit.getScheduler());
                    }
                }
            }

        }
        catch (ClassNotFoundException | IllegalAccessException e)
        {
            e.printStackTrace();
            return null;
        }

        return Aurora.mainThread;
    }
}
