package net.centurylab.aurora.events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.event.HandlerList;

import net.centurylab.aurora.events.definedevents.creatures.DisallowCreatureSpawn;
import net.centurylab.aurora.events.definedevents.creatures.DisallowSpecificCreatureSpawn;
import net.centurylab.aurora.events.definedevents.player.PlayerCanBreakPlacedBlocks;
import net.centurylab.aurora.events.definedevents.player.PlayerCanBreakPlacedBlocksAndSpecificMaterial;
import net.centurylab.aurora.singleton.SingletonManager;

public class EventManager
{
    private static DisallowSpecificCreatureSpawn                 disallowSpecificCreatureSpawn                 = null;
    private static DisallowCreatureSpawn                         disallowCreatureSpawn                         = null;
    private static PlayerCanBreakPlacedBlocks                    playerCanBreakPlacedBlocks                    = null;
    private static PlayerCanBreakPlacedBlocksAndSpecificMaterial playerCanBreakPlacedBlocksAndSpecificMaterial = null;

    public static void dissallowCreatureSpawn() throws Exception
    {
        if (disallowCreatureSpawn == null)
        {
            disallowCreatureSpawn = new DisallowCreatureSpawn();
        }

        Bukkit.getPluginManager().registerEvents(disallowCreatureSpawn, SingletonManager.getPlugin());
    }

    public static void allowCreatureSpawn()
    {
        if (disallowCreatureSpawn != null)
        {
            HandlerList.unregisterAll(disallowCreatureSpawn);
        }

        if (disallowSpecificCreatureSpawn != null)
        {
            HandlerList.unregisterAll(disallowSpecificCreatureSpawn);
        }
    }

    public static void disallowSpecificCreatureSpawn(EntityType... entityTypes) throws Exception
    {
        if (disallowSpecificCreatureSpawn == null)
        {
            disallowSpecificCreatureSpawn = new DisallowSpecificCreatureSpawn(entityTypes);
        }

        Bukkit.getPluginManager().registerEvents(disallowSpecificCreatureSpawn, SingletonManager.getPlugin());
    }

    /**
     * Sets if the player can only break placed blocks
     */
    public static void allowPlayerCanBreakPlacedBlocks() throws Exception
    {
        if (playerCanBreakPlacedBlocks == null)
        {
            playerCanBreakPlacedBlocks = new PlayerCanBreakPlacedBlocks();
        }

        Bukkit.getPluginManager().registerEvents(playerCanBreakPlacedBlocks, SingletonManager.getPlugin());
    }

    /**
     * Sets if the players can break all blocks
     */
    public static void disallowPlayerCanBreakPlacedBlocks()
    {
        if (playerCanBreakPlacedBlocks == null)
        {
            return;
        }

        HandlerList.unregisterAll(playerCanBreakPlacedBlocks);
    }

    public static void allowPlayerCanBreakPlacedBlocksAndSpecificMaterial(Material... materials) throws Exception
    {
        if (playerCanBreakPlacedBlocksAndSpecificMaterial == null)
        {
            playerCanBreakPlacedBlocksAndSpecificMaterial = new PlayerCanBreakPlacedBlocksAndSpecificMaterial(materials);
        }

        Bukkit.getPluginManager().registerEvents(playerCanBreakPlacedBlocksAndSpecificMaterial, SingletonManager.getPlugin());
    }

    public static void disallowPlayerCanBreakPlacedBlocksAndSpecificMaterial()
    {
        if (playerCanBreakPlacedBlocksAndSpecificMaterial == null)
        {
            return;
        }

        HandlerList.unregisterAll(playerCanBreakPlacedBlocksAndSpecificMaterial);
    }
}
