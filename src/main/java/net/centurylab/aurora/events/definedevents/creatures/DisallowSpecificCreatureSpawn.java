package net.centurylab.aurora.events.definedevents.creatures;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

import java.util.List;

public class DisallowSpecificCreatureSpawn implements Listener
{
    private List<EntityType> entityTypes;

    public DisallowSpecificCreatureSpawn(EntityType... entityTypes)
    {
        for (EntityType entityType : entityTypes)
        {
            if (!this.entityTypes.contains(entityType))
            {
                this.entityTypes.add(entityType);
            }
        }
    }

    @EventHandler
    public void onCreatureSpawn(CreatureSpawnEvent event)
    {
        if (this.entityTypes.contains(event.getEntityType()))
        {
            event.setCancelled(true);
        }
    }
}
