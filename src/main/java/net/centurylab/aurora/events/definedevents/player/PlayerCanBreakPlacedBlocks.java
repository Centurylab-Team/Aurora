package net.centurylab.aurora.events.definedevents.player;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.ArrayList;
import java.util.List;

public class PlayerCanBreakPlacedBlocks implements Listener
{
    List<Block> placedBlocks = new ArrayList<>();

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event)
    {
        this.placedBlocks.add(event.getBlockPlaced());
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event)
    {
        if (!this.placedBlocks.contains(event.getBlock()))
        {
            event.setCancelled(true);
        }

        if (this.placedBlocks.contains(event.getBlock()))
        {
            this.placedBlocks.remove(event.getBlock());
        }
    }

    @EventHandler
    public void onBlockDamage(BlockDamageEvent event)
    {
        if (!this.placedBlocks.contains(event.getBlock()))
        {
            event.setCancelled(true);
        }
        if (this.placedBlocks.contains(event.getBlock()))
        {
            this.placedBlocks.remove(event.getBlock());
        }
    }
}
