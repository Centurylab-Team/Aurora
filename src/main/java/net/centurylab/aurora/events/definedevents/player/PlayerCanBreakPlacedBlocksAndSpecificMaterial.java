package net.centurylab.aurora.events.definedevents.player;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.List;

public class PlayerCanBreakPlacedBlocksAndSpecificMaterial implements Listener
{
    private List<Material> materials;
    private List<Block> placedBlocks;

    public PlayerCanBreakPlacedBlocksAndSpecificMaterial(Material... materials)
    {
        for (Material material : materials)
        {
            if (!this.materials.contains(material))
            {
                this.materials.add(material);
            }
        }
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event)
    {
        this.placedBlocks.add(event.getBlockPlaced());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event)
    {
        if (!this.materials.contains(event.getBlock().getType()) && !this.placedBlocks.contains(event.getBlock()))
        {
            event.setCancelled(true);
            return;
        }

        if (this.placedBlocks.contains(event.getBlock()))
        {
            this.placedBlocks.remove(event.getBlock());
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockDamage(BlockDamageEvent event)
    {
        if (!this.materials.contains(event.getBlock().getType()) && !this.placedBlocks.contains(event.getBlock()))
        {
            event.setCancelled(true);
            return;
        }

        if (this.placedBlocks.contains(event.getBlock()))
        {
            this.placedBlocks.remove(event.getBlock());
        }
    }
}
