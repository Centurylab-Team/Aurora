package net.centurylab.aurora.inventory;

import org.bukkit.entity.HumanEntity;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.InventoryView;

/**
 * @author ar56te876mis <ar56te876mis@yahoo.de>
 */
public abstract class WrapperInventory implements InventoryHolder
{
    public final void onInventoryClick(InventoryClickEvent event)
    {
        boolean cancel;
        HumanEntity player = event.getWhoClicked();
        InventoryView view = event.getView();
        int rawSlot = event.getRawSlot();
        onClick(player, view, rawSlot);
        switch (event.getClick())
        {
            case CONTROL_DROP:
                cancel = onControlDrop(player, view, rawSlot);
                break;
            case CREATIVE:
                cancel = onCreativeClick(player, view, rawSlot);
                break;
            case DOUBLE_CLICK:
                cancel = onDoubleClick(player, view, rawSlot);
                break;
            case DROP:
                cancel = onDrop(player, view, rawSlot);
                break;
            case LEFT:
                cancel = onLeftClick(player, view, rawSlot);
                break;
            case MIDDLE:
                cancel = onMiddelClick(player, view, rawSlot);
                break;
            case NUMBER_KEY:
                cancel = onNumberPress(player, view, rawSlot);
                break;
            case RIGHT:
                cancel = onRightClick(player, view, rawSlot);
                break;
            case SHIFT_LEFT:
                cancel = onShiftLeftClick(player, view, rawSlot);
                break;
            case SHIFT_RIGHT:
                cancel = onShiftRightClick(player, view, rawSlot);
                break;
            case WINDOW_BORDER_LEFT:
                cancel = onBorderLeftClick(player, view, rawSlot);
                break;
            case WINDOW_BORDER_RIGHT:
                cancel = onBorderRightClick(player, view, rawSlot);
                break;
            default:
                cancel = true;
                break;
        }
        if (cancel)
        {
            event.setCancelled(true);
        }
    }

    public boolean defaultCancel(HumanEntity player, InventoryView view, int rawSlot, ClickType clickType)
    {
        if (clickType == ClickType.SHIFT_LEFT || clickType == ClickType.SHIFT_RIGHT)
        {
            return true;
        }
        return rawSlot != -999 && isTopInventory(rawSlot, getInventory().getSize());
    }

    public void onClick(HumanEntity player, InventoryView view, int rawSlot)
    {

    }

    public boolean onControlDrop(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.CONTROL_DROP);
    }

    public boolean onCreativeClick(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.CREATIVE);
    }

    public boolean onDoubleClick(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.DOUBLE_CLICK);
    }

    public boolean onDrop(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.DROP);
    }

    public boolean onLeftClick(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.LEFT);
    }

    public boolean onMiddelClick(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.MIDDLE);
    }

    public boolean onNumberPress(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.NUMBER_KEY);
    }

    public boolean onRightClick(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.RIGHT);
    }

    public boolean onShiftLeftClick(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.SHIFT_LEFT);
    }

    public boolean onShiftRightClick(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.SHIFT_RIGHT);
    }

    public boolean onBorderLeftClick(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.WINDOW_BORDER_LEFT);
    }

    public boolean onBorderRightClick(HumanEntity player, InventoryView view, int rawSlot)
    {
        return defaultCancel(player, view, rawSlot, ClickType.WINDOW_BORDER_RIGHT);
    }

    public boolean onOpen(HumanEntity player, InventoryView view)
    {
        return false;
    }

    public void onClose(HumanEntity player, InventoryView view)
    {

    }

    protected boolean isTopInventory(int raw, int size)
    {
        return raw < size;
    }
}
