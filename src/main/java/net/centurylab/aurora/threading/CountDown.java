package net.centurylab.aurora.threading;

import com.google.common.base.Preconditions;

import org.bukkit.Bukkit;

import net.centurylab.aurora.callbacks.threading.CountdownCallback;
import net.centurylab.aurora.singleton.SingletonManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CountDown implements Runnable
{
    private Map<Integer, List<CountdownCallback>> callbacks;
    private int                                   countdown;
    private int                                   threadId;

    public CountDown(int countdown)
    {
        Preconditions.checkArgument(countdown > 0, "countdown must be a positive value");

        this.callbacks = new HashMap<>();
        this.countdown = countdown;
        this.threadId = 0;
    }


    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run()
    {
        if (this.threadId == 0 || this.threadId == -1)
        {
            return;
        }

        if (this.callbacks.containsKey(countdown))
        {
            for (CountdownCallback countdownCallback : this.callbacks.get(this.countdown))
            {
                countdownCallback.onTick(this.countdown);
            }

        }

        this.countdown--;

        if (this.countdown == -1)
        {
            Bukkit.getScheduler().cancelTask(this.threadId);
        }
    }

    public void start() throws Exception
    {
        this.threadId = Bukkit.getScheduler().scheduleSyncRepeatingTask(SingletonManager.getPlugin(), this, 0, 20);

        Preconditions.checkArgument(this.threadId > 0, "Can't start thread");
    }

    public void stop() throws Exception
    {
        Preconditions.checkArgument(this.threadId > 0, "Can't stop thread");

        Bukkit.getScheduler().cancelTask(this.threadId);
    }

    public boolean isStarted()
    {
        return this.threadId > 0;
    }

    public void addCallback(int seconds, CountdownCallback countdownCallback)
    {
        Preconditions.checkArgument(seconds > 0, "seconds must be a positive value");
        Preconditions.checkNotNull(countdownCallback, "countdownCallback can't be null");

        if (!this.callbacks.containsKey(seconds))
        {
            this.callbacks.put(seconds, new ArrayList<CountdownCallback>());
        }

        this.callbacks.get(seconds).add(countdownCallback);
    }

    public void addCallback(int[] seconds, CountdownCallback countdownCallback)
    {
        Preconditions.checkNotNull(seconds, "seconds can't be null");
        Preconditions.checkNotNull(countdownCallback, "countdownCallback can't be null");

        for (int second : seconds)
        {
            if (!this.callbacks.containsKey(seconds))
            {
                this.callbacks.put(second, new ArrayList<CountdownCallback>());
            }

            this.callbacks.get(second).add(countdownCallback);
        }
    }
}
