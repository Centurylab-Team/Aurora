package net.centurylab.aurora.singleton;

import com.google.common.base.Preconditions;

import org.bukkit.plugin.java.JavaPlugin;

import net.centurylab.aurora.player.AuroraPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class SingletonManager
{
    public static final List<AuroraPlayer> AURORA_PLAYERS = new ArrayList<>();
    private static      JavaPlugin         plugin         = null;
    private static      Logger             logger         = null;

    public static JavaPlugin getPlugin() throws Exception
    {
        Preconditions.checkNotNull(plugin, "Your JavaPlugin is null. Initialize the framework with \"Framework.init(this, null);\" in your JavaPlugin#onEnable()");

        return plugin;
    }

    public static void setPlugin(JavaPlugin plugin)
    {
        SingletonManager.plugin = plugin;
        logger = plugin.getLogger();
    }

    public static Logger getLogger()
    {
        return logger;
    }
}
