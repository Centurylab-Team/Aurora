package net.centurylab.aurora.items;

import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFactory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

import java.util.List;
import java.util.Map;

public class ItemStackBuilder
{
    private ItemStack    itemStack;
    private ItemMeta     itemMeta;
    private MaterialData materialData;

    /**
     * private constructor
     */
    private ItemStackBuilder()
    {
        this.itemStack = new ItemStack(Material.AIR, 0);
        this.itemMeta = itemStack.getItemMeta();
        this.materialData = itemStack.getData();
    }

    /**
     * Returns a new instance of the ItemStackBuilder
     *
     * @return New instance of ItemStackBuilder
     */
    public static ItemStackBuilder builder()
    {
        return new ItemStackBuilder();
    }

    /**
     * Returns new ItemStackBuilder with the given arguments
     *
     * @param material the type
     * @param amount   the amount in the stack
     * @param data     the data value or null
     * @return New instance of ItemStackBuilder with the given arguments
     */
    public static ItemStackBuilder builder(Material material, int amount, byte data)
    {
        return new ItemStackBuilder().setType(material).setAmount(amount).setData(data);
    }

    /**
     * Returns the custom ItemStack for a player skull
     *
     * @param skullOwner  Name of the player
     * @param amount      Specific amount
     * @param displayname Displayname of the itemstack
     * @return ItemStack of the player skull
     */
    public static ItemStack getPlayerSkull(String skullOwner, int amount, String displayname)
    {
        if (amount < 0)
        {
            amount = 1;
        }

        ItemStack itemStack = new ItemStack(Material.SKULL_ITEM, amount, (short) SkullType.PLAYER.ordinal());

        SkullMeta skullMeta = (SkullMeta) itemStack.getItemMeta();

        skullMeta.setOwner(skullOwner);

        if (displayname != null)
        {
            skullMeta.setDisplayName(displayname);
        }

        itemStack.setItemMeta(skullMeta);

        return itemStack;
    }

    /**
     * Returns the custom ItemStack for a player skull
     *
     * @param skullOwner Name of the plaer
     * @param amount     Specific amount
     * @return ItemStack of the player skull
     */
    public static ItemStack getPlayerSkull(String skullOwner, int amount)
    {
        return getPlayerSkull(skullOwner, amount, null);
    }

    /**
     * Returns the custom ItemStack for a player skull
     *
     * @param skullOwner  Name of the plaer
     * @param displayname Displayname of the itemstack
     * @return ItemStack of the player skull
     */
    public static ItemStack getPlayerSkull(String skullOwner, String displayname)
    {
        return getPlayerSkull(skullOwner, 1, displayname);
    }

    /**
     * Returns the custom ItemStack for a player skull
     *
     * @param skullowner Name of the player
     * @return ItemStack of the player skull
     */
    public static ItemStack getPlayerSkull(String skullowner)
    {
        return getPlayerSkull(skullowner, 1, null);
    }

    /**
     * Sets the type of this item
     * <p>
     * Note that in doing so you will reset the MaterialData for this stack
     *
     * @param type New type to set the items in this stack to
     */
    public ItemStackBuilder setType(Material type)
    {
        itemStack.setType(type);
        materialData = new MaterialData(type);

        return this;
    }

    /**
     * Sets the type id of this item
     * <p>
     * Note that in doing so you will reset the MaterialData for this stack
     *
     * @param type New type id to set the items in this stack to
     * @deprecated Magic value
     */
    @Deprecated
    public ItemStackBuilder setTypeId(int type)
    {
        itemStack.setTypeId(type);

        return this;
    }

    /**
     * Sets the amount of items in this stack
     *
     * @param amount New amount of items in this stack
     */
    public ItemStackBuilder setAmount(int amount)
    {
        itemStack.setAmount(amount);

        return this;
    }

    /**
     * Sets the MaterialData for this stack of items
     *
     * @param data New MaterialData for this item
     */
    public ItemStackBuilder setData(MaterialData data)
    {
        itemStack.setData(data);

        return this;
    }

    /**
     * Sets the raw data of this material
     *
     * @param data New raw data
     * @deprecated Magic value
     */
    public ItemStackBuilder setData(byte data)
    {
        materialData.setData(data);

        return this;
    }

    /**
     * Sets the durability of this item
     *
     * @param durability Durability of this item
     */
    public ItemStackBuilder setDurability(short durability)
    {
        itemStack.setDurability(durability);

        return this;
    }

    /**
     * Adds the specified {@link Enchantment} to this item stack.
     * <p>
     * If this item stack already contained the given enchantment (at any
     * level), it will be replaced.
     *
     * @param ench  Enchantment to add
     * @param level Level of the enchantment
     * @throws IllegalArgumentException if enchantment null, or enchantment is
     *                                  not applicable
     */
    public ItemStackBuilder addEnchantment(Enchantment ench, int level)
    {
        itemStack.addEnchantment(ench, level);

        return this;
    }

    /**
     * Adds the specified enchantments to this item stack in an unsafe manner.
     * <p>
     * This method is the same as calling {@link
     * #addUnsafeEnchantment(Enchantment, int)} for
     * each element of the map.
     *
     * @param enchantments Enchantments to add
     */
    public ItemStackBuilder addUnsafeEnchantments(Map<Enchantment, Integer> enchantments)
    {
        itemStack.addUnsafeEnchantments(enchantments);

        return this;
    }

    /**
     * Adds the specified {@link Enchantment} to this item stack.
     * <p>
     * If this item stack already contained the given enchantment (at any
     * level), it will be replaced.
     * <p>
     * This method is unsafe and will ignore level restrictions or item type.
     * Use at your own discretion.
     *
     * @param ench  Enchantment to add
     * @param level Level of the enchantment
     */
    public ItemStackBuilder addUnsafeEnchantment(Enchantment ench, int level)
    {
        itemStack.addUnsafeEnchantment(ench, level);

        return this;
    }

    /**
     * Set the ItemMeta of this ItemStack.
     *
     * @param itemMeta new ItemMeta, or null to indicate meta data be cleared.
     * @return True if successfully applied ItemMeta, see {@link
     * ItemFactory#isApplicable(ItemMeta, ItemStack)}
     * @throws IllegalArgumentException if the item meta was not created by
     *                                  the {@link ItemFactory}
     */
    public ItemStackBuilder setItemMeta(ItemMeta itemMeta) throws Exception
    {
        if (!itemStack.setItemMeta(itemMeta))
        {
            throw new Exception("ItemMeta can't be applied to Itemstack");
        }

        return this;
    }

    /**
     * Sets the display name.
     *
     * @param name the name to set
     */
    public ItemStackBuilder setDisplayName(String name)
    {
        itemMeta.setDisplayName(name);

        return this;
    }

    /**
     * Sets the lore for this item.
     * Removes lore when given null.
     *
     * @param lore the lore that will be set
     */
    public ItemStackBuilder setLore(List<String> lore)
    {
        itemMeta.setLore(lore);

        return this;
    }

    /**
     * Set itemflags which should be ignored when rendering a ItemStack in the Client. This Method does silently ignore double set itemFlags.
     *
     * @param itemFlags The hideflags which shouldn't be rendered
     */
    public ItemStackBuilder addItemFlags(ItemFlag... itemFlags)
    {
        itemMeta.addItemFlags(itemFlags);

        return this;
    }

    /**
     * Returns the custom ItemStack with all set attributes
     *
     * @return The built item
     */
    public ItemStack build()
    {
        this.itemStack.setItemMeta(this.itemMeta);
        this.itemStack.setData(this.materialData);

        return this.itemStack;
    }
}
